"use strict"

Voodoo.registerProperty("position.position", "parent", function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	switch(properties.position.position.toLowerCase()){
		case 'parent':
			element.css('position','absolute');
			break;
		case 'page':
			element.css('position','fixed');
			break;
		default:
			element.css('position','relative');
	}
});

Voodoo.registerProperty("position.width", "auto", function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	var hasWidth = widgetProperties.hasOwnProperty('position') && widgetProperties.position.hasOwnProperty('width');
	
	if( hasWidth || !properties.position.hasOwnProperty('left') || !properties.position.hasOwnProperty('right')){
		element.css('width',properties.position.width);
	}
});

Voodoo.registerProperty("position.height", "auto", function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	var hasHeight = widgetProperties.hasOwnProperty('position') && widgetProperties.position.hasOwnProperty('height');
	
	if( hasHeight || !properties.position.hasOwnProperty('top') || !properties.position.hasOwnProperty('bottom')){
		element.css('height',properties.position.height);
	}
});

Voodoo.registerProperty("position.left", 0, function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	element.css('left',properties.position.left + properties.margin.left);
});

Voodoo.registerProperty("position.right", 0, function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	var hasWidth = widgetProperties.hasOwnProperty('position') && widgetProperties.position.hasOwnProperty('width');
	
	if(!hasWidth){
		element.css('right',properties.position.right + properties.margin.right);
	}
});

Voodoo.registerProperty("position.top", 0, function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	element.css('top',properties.position.top + properties.margin.top);
});

Voodoo.registerProperty("position.bottom", 0, function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	var hasHeight = widgetProperties.hasOwnProperty('position') && widgetProperties.position.hasOwnProperty('height');
	
	if(!hasHeight){
		element.css('bottom',properties.position.bottom + properties.margin.bottom);
	}
});

Voodoo.registerProperty("margin.left", 0);
Voodoo.registerProperty("margin.right", 0);
Voodoo.registerProperty("margin.top", 0);
Voodoo.registerProperty("margin.bottom", 0);


Voodoo.registerProperty("background.color", Voodoo.theme.color.background || 'white', function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	element.css('background-color',properties.background.color);
});


Voodoo.registerProperty("scroll.horizontal", "clip", function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	switch(properties.scroll.horizontal.toLowerCase()){
		default:
		case 'clip':
			element.css('overflow-x','hidden');
			break;
		case 'scroll':
			element.css('overflow-x','scroll');
			break;
		case 'auto':
			element.css('overflow-x','auto');
			break;
	}
});

Voodoo.registerProperty("scroll.vertical", "clip", function(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile){
	switch(properties.scroll.vertical.toLowerCase()){
		default:
		case 'clip':
			element.css('overflow-y','hidden');
			break;
		case 'scroll':
			element.css('overflow-y','scroll');
			break;
		case 'auto':
			element.css('overflow-y','auto');
			break;
	}
});

Voodoo.angularModule.controller('VoodooCtrl',function($scope){

	$scope.layout = {
	
		children:[
		
			{
				widget:'view',
				properties:{
					background:{
						color:'purple'
					},
					scroll:{
						horizontal:'auto',
						vertical:'auto'
					}
					
				},
				layout:{
					children:[
						{
							widget:'view',
							properties:{
								background:{
									color:'red'
								},
								position:{
									left:350,
									width:400
								}
							},
							layout:{
								children:[]
								
							}
						},
						{
							widget:'view',
							properties:{
								background:{
									color:'blue'
								},
								position:{
									left:800,
									width:'400',
									height:'400'
								}
							},
							layout:{
								children:[]
								
							}
						}
					]
				}
			}
		]
	
	};



});

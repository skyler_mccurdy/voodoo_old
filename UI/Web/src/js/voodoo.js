"use strict"
var Voodoo = {};
Voodoo.angularModule = angular.module('voodoo',[]);

Voodoo.coalesce = function(baseProperties,newProperties){
		
	var result = jQuery.extend(true,{},baseProperties);
	result = jQuery.extend(true,result,newProperties);
	
	return result;
};

Voodoo.theme = {

	color:{
		default:'black',
		primary:'blue',
		warning:'yellow',
		alert:'red',
		background:'#EEEEEE'
	}
};

Voodoo._defaultProperties = {};
Voodoo._propertySetters = {};

Voodoo.registerProperty = function(propertyName, defaultValue, setMethod){

	if(typeof(propertyName) !== "string"){
		throw "propertyName must be a string";
	}
	
	if(typeof(setMethod) !== "function" && setMethod){
		throw "setMethod must be a function";
	}
	
	var nameParts = propertyName.split('.');
	
	var defaults = Voodoo._defaultProperties;
	var setters = Voodoo._propertySetters;
	
	for(var i=0;i<nameParts.length-1;i++){
		if(!defaults.hasOwnProperty(nameParts[i])){
			defaults[nameParts[i]] = {};
		}
		defaults = defaults[nameParts[i]];
		
		if(!setters.hasOwnProperty(nameParts[i])){
			setters[nameParts[i]]={};
		}
		setters = setters[nameParts[i]];
	}
	
	if(typeof(defaults[nameParts[nameParts.length-1]])!='undefined'){
		throw "Default property already set for "+propertyName;
	}
	
	if(typeof(setters[nameParts[nameParts.length-1]])!='undefined'){
		throw "Property setter already set for "+propertyName;
	}
	
	defaults[nameParts[i]] = defaultValue;
	setters[nameParts[i]] = setMethod;
};

Voodoo.applyProperties = function(widgetProperties,parentWidgetProperties,element,container,$compile){

	var properties = widgetProperties || {};
	properties = Voodoo.coalesce(Voodoo._defaultProperties,properties);
	
	var parentProperties = parentWidgetProperties || {};
	parentProperties = Voodoo.coalesce(Voodoo._defaultProperties,parentProperties);
	
	Voodoo._applyProperty(properties,widgetProperties,parentProperties,parentWidgetProperties,Voodoo._propertySetters,element,container,$compile,'');
}

Voodoo._applyProperty = function(properties,widgetProperties,parentProperties,parentWidgetProperties,setters,element,container,$compile,propertyName){

	var prop = null;
	if(propertyName == ''){
		prop = properties;
	}else{
		prop = Voodoo.getValue(properties,propertyName);
	}
	
	if(prop==null){
		throw "Unknown property "+propertyName;
	}
	
	
	if(typeof(prop)=='object'){
		for(var key in prop){
			if(prop.hasOwnProperty(key)){
				var pname = propertyName;
				if(pname.length>0){
					pname+='.'+key;
				}else{
					pname = key;
				}
				Voodoo._applyProperty(properties,widgetProperties,parentProperties,parentWidgetProperties,setters,element,container,$compile,pname);
			}
		}
	}else{
		var setter = Voodoo.getValue(setters,propertyName);
		if(typeof(setter)=='function'){
			setter(element,container,properties,widgetProperties,parentProperties,parentWidgetProperties,$compile);
		}
	}
}

Voodoo.getValue = function(properties,propertyName){

	if(typeof(propertyName)!='string'){
		throw "propertyName must be string";
	}
	
	if(typeof(properties)=='undefined'){
		return null;
	}
	
	if(typeof(properties)!='object'){
		throw "properties must be an object";
	}
	
	var nameParts = propertyName.split('.');
	
	var current = properties;
	for(var i=0;i<nameParts.length-1;i++){
		if(current!=null && current.hasOwnProperty(nameParts[i])){
			current = current[nameParts[i]];
		}else{
			return null;
		}
	}
	
	return current[nameParts[nameParts.length-1]];
}





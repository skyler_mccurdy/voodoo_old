

{

	for(var w in Voodoo.widgets){
	
		function reg(widgetName){
		
			Voodoo.angularModule.directive(widgetName+'Widget', function($compile){

				var widget = Voodoo.widgets[widgetName]($compile);

				return {
					restrict: 'E',
					scope:{
						parent:'=',
						properties:'=',
						data:'=',
						layout:'='
					},
					link: widget.link,
					controller:widget.controller
		
				};

			});
		};
		
		reg(w);
	
	}

}

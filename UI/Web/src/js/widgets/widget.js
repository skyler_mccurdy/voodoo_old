"use strict"

Voodoo.widgets = {};
Voodoo.widgets.widget = function($compile){

	var widget = {};

	widget.widgetName = "widget";
	
	widget.html = '<div class="widget-widget"><container></container></widget>';
	
	widget.controller = function($scope){
			
		$scope.scope = $scope;
		
		$scope.children = [];
		
		if($scope.parent && $scope.parent.children){
			$scope.parent.children[$scope.parent.children.length]=$scope;
		}
			
	};
	
	widget.link = function(scope,element,attrs){
	
		var compiledHtml = $compile(widget.html)(scope);
		element.html(compiledHtml);
		
		var container = element.find('container');
	
		scope.$watch(function(){ return scope.properties; },function(){
			Voodoo.applyProperties(scope.properties,null,element,container,null);
		},true);
			
		scope.$watchCollection(scope.layout.children,function(){				
			if(scope.layout.children){
				
				container.html('');
				for(var i=0;i<scope.layout.children.length;i++){
					widget.addChild(scope,container,scope.layout.children[i],'layout.children['+i+']');
				}
			}
		});		
	};
	
	widget.addChild = function(scope,container,child,selector){
	
		if(Voodoo.widgets.hasOwnProperty(child.widget)){
			var directive = '<'+child.widget+'-widget parent="scope" properties="'+selector+'.properties" layout="'+selector+'.layout"></'+child.widget+'-widget>';
			var compiledHtml = $compile(directive)(scope);
			container.append(compiledHtml);
		}else{
			throw "Unknown Widget "+child.widget;
		}
		
	};
	
	return widget;
};

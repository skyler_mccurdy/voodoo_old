"use strict"

Voodoo.widgets.view = function($compile){

	
	var view = Voodoo.widgets.widget($compile);
	var base = {};

	view.widgetName = "View";
	
	view.html = '<div class="view-widget"><container></container></div>';
	
	view.viewProperties = {
		position:{
			position:'parent',
			left:0,
			top:0,
			right:0,
			bottom:0
		}
	};

	base.controller=view.controller;
	view.controller = function($scope){
		base.controller.call(this,$scope);
		$scope.data += " tits";
	};
	
	
	view.defaultProperties = Voodoo.coalesce(view.defaultProperties,view.viewProperties);

	return view;
};

